describe('toPromise helper', () => {
  const PARAMS = '__PARAMS__';
  let toPromise;

  beforeEach(function() {
    jasmine.addMatchers(require('jasmine-node-promise-matchers'));
  });

  beforeEach(() => {
    toPromise = require('../../src/helpers/toPromise');
  });

  it('returns resolved promise on success', (done) => {
    let method = (args, success, error) => success(args);
    let result = toPromise(method, PARAMS);

    expect(result).toResolveWith(PARAMS, done);
  });

  it('returns resolved promise on error', (done) => {
    let method = (args, success, error) => error();
    let result = toPromise(method, PARAMS);

    expect(result).toReject(done);
  });
});
