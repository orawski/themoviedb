describe('movieDb service', () => {
  const proxyquire = require('proxyquire');

  const IMAGES_URI = '__IMAGES_URI__';
  const THE_MOVIE_DB = { common: { images_uri: IMAGES_URI }, search: { getMovie() {} } };
  const CONFIG = {
    API_KEY: '__API_KEY_'
  };
  const DATA = '{"a":"b"}';
  const DECODED_DATA = { a: "b" };

  let movieDb;
  let getMovieSpy;

  beforeEach(function() {
    jasmine.addMatchers(require('jasmine-node-promise-matchers'));
  });

  beforeEach(() => {
    global.theMovieDb = THE_MOVIE_DB;
    movieDb = proxyquire('../../src/services/movieDb', {
      '../config/config': CONFIG
    });
  });

  it('sets proper api key from config', () => {
    expect(global.theMovieDb.common.api_key).toBe(CONFIG.API_KEY);
  });

  describe('common', () => {
    it('returns proper images uri', () => {
      expect(movieDb.common.images_uri).toEqual(IMAGES_URI);
    });
  });

  describe('search.getMovieSpy method', () => {
    beforeEach(() => {
      getMovieSpy = spyOn(global.theMovieDb.search, 'getMovie');
    });

    it('returns promise resolving to decoded data on success', (done) => {
      getMovieSpy.and.callFake((parameters, success, error) => { success(DATA) });
      let result = movieDb.search.getMovie('parameters');

      expect(result).toResolveWith(DECODED_DATA, done);
    });

    it('returns rejecting promise on error', (done) => {
      getMovieSpy.and.callFake((parameters, success, error) => { error() });
      let result = movieDb.search.getMovie('parameters');

      expect(result).toReject(done);
    });
  });
});
