require([
  './components/search_factory/search_factory',
  './components/movie_factory/movie_factory',
], (SearchFactory, MovieFactory) => {
  let movieFactory = new MovieFactory();
  let searchFactory = new SearchFactory(movieFactory);

  let search = searchFactory.build();

  search.bind(document.querySelector('#search'), document.querySelector('#movies'));
});
