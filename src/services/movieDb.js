if (typeof define !== 'function') { var define = require('amdefine')(module) }

define([
  '../config/config',
  '../helpers/toPromise',
  '../../node_modules/themoviedb-javascript-library/themoviedb'
], (config, toPromise) => {
  theMovieDb.common.api_key = config.API_KEY;

  return {
    common: {
      images_uri: theMovieDb.common.images_uri
    },
    search: {
      getMovie(parameters) {
        return toPromise(theMovieDb.search.getMovie, parameters)
          .then(data => JSON.parse(data));
      }
    }
  };
});
