define(() => (title, overview, posterUrl) => `
  <img class="image" src="$3">
  <span class="title">$1</span>
  <span class="overview">$2</span>
`.replace('$1', title).replace('$2', overview).replace('$3', posterUrl));
