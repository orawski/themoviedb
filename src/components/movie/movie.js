define([
  '../../services/movieDb',
  '../../config/config',
  './template'
], (movieDb, config, template) => function Movie(title, overview, _posterUrl_) {
  let posterUrl = movieDb.common.images_uri + config.IMG_SIZE + _posterUrl_;

  this.render = (element) => {
    let div = document.createElement('div');

    div.innerHTML = template(title, overview, posterUrl);
    element.appendChild(div);
  }
});
