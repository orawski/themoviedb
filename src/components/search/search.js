define(['../../services/movieDb'], (movieDb) => {
  const DEBOUNCE = 500;

  return function Search(movieFactory) {
    let searchElement, moviesElement;

    this.bind = (_searchElement_, _moviesElement_) => {
      let timeout;

      searchElement = _searchElement_;
      moviesElement = _moviesElement_;

      searchElement.addEventListener("input", function () {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
          search(searchElement.value);
        }, DEBOUNCE);
      });
    };

    function search(query) {
      let data = movieDb.search.getMovie({ query }).then(data => {
        clear();
        render(data.results);
      });
    }

    function clear() {
      moviesElement.innerHTML = '';
    }

    function render(movies) {
      movies.forEach(movie => {
        movieFactory.build(movie).render(moviesElement);
      });
    }
  };
});
