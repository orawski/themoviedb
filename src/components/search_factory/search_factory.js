define(['../search/search'], (Search) => {
  return function SearchFactory(movieFactory) {
    this.build = () => new Search(movieFactory);
  }
});
