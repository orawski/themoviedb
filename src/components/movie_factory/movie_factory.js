define(['../movie/movie'], (Movie) => {
  return function MovieFactory() {
    this.build = (movie) => new Movie(movie.title, movie.overview, movie.poster_path);
  }
});
