if (typeof define !== 'function') { var define = require('amdefine')(module) }

define(() => {
  // transforms a callback based method (second and third argument are callbacks
  // for success and error respectively) into a promise
  return function toPromise(method, ...args) {
    return new Promise((resolve, reject) => {
      method(...args, (data) => resolve(data), () => reject());
    });
  }
});
